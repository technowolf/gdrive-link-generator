# Gdrive link generator

Generates a direct download link for Google Drive file.

[Webpage Link](https://technowolf.gitlab.io/gdrive-link-generator)
