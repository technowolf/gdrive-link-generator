$(document).ready(function () {
    var $baseDownloadLink = $('#baseDownloadLink'),
        $downloadLink = $('#processedDownloadLink'),
        $copyButton = $('#copyProcessedLink');

    $baseDownloadLink.on('keyup paste', function () {
        var baseLink = $baseDownloadLink.val(),
            regexLink = baseLink.replace(/\/file\/d\/(.+)\/(.+)/, "/uc?export=download&id=$1");
        if (regexLink !== baseLink) {
            $downloadLink.val(regexLink);
            $copyButton.removeAttr('disabled');
        } else {
            $downloadLink.val('');
            $copyButton.attr('disabled', 'disabled');
        }
    });

    $downloadLink.on('click', function () {
        $downloadLink.select();
    });

    $copyButton.on("click", function () {
        new ClipboardJS('#copyProcessedLink');
    });

});
